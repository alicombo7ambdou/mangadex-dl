# mangadex-dl

Utitlity functions for downloading definite chapters (and whole manga) off of mangadex.

## Requirements

* bash
* uuidgen
* curl (built with http2 support)
* jq

Optional stuff:

* tar (save as `cbt`)
* zip (save as `cbz`)
* feh (view image folder)
* evince (view `.cbt`/`.cbz`)

## Usage

Add commands to your shell:

```shell
. mdx_dl.sh
```

## Environment Variables

There are three environment variables currently in use:

* `mangadex_groups`: This is self-generated and self filled; and used to insert folder names; is an indexed array.
* `SAVEAS`: Used to default the save style when running `mangadex_chdl_single`. See command usage `mangadex_chdl_single` for information.
* `LANG_CODE`: Used to specify only to download chapters in this language code. It is not the normal ISO 639 languages you know of; but the "usually spoken" languages in ISO 3166 alpha-2. For example, `LANG_CODE=gb` is English, `LANG_CODE=mx` is spanish and so on. If you're pissed off about it shout at Holo; not me. If they change it then this is already out of date.

## Command Usage

```shell
$ mangadex-dl-chapter-single $chapter_id "$manga_title" "$SAVEAS"
```

`mangadex-dl-chapter-single` takes three parameters; two optional:

* `int chapter_id`: required. An existing manga chapter id on mangadex.
* `string manga_title`: optional. Manga title; used to avoid a round-trip.
* `string SAVEAS`: optional. What to save as. Meaningful are: `tar`/`cbt` (`.cbt`), `zip`/`cbz` (`.cbz`) and `mv`/`folder` (saved as a folder on the system). Defaults to the environment variable `$SAVEAS` or `cbt` if not specified.

```shell
$ mangadex-dl-chapters $@
```

Takes an infinite number of parameters.

* `...int chapter_id`: existing manga chapter ID on mangadex.

Relies on the `$SAVEAS` variable; and will always make a minimum of 2 requests to mangadex.org for chapter and title data.

```shell
$ mangadex-dl-manga $@
```

Takes an infinite number of parameters.

* `...int manga_id`: manga ID on mangadex.

Relies on the `$SAVEAS` and `$LANG_CODE` variables. If `$LANG_CODE` is not set (or set to a blank value) then it is not followed.

## Save format

Chapters are always saved in the `$manga_name/$chapter_name` format; but the format of `$manga_name` is constant (literally the name of the manga; except when there's a `/`; which is replaced with underscores.

The generalised format in bash/perl style regex is as follows:

```bash-regex
([a-z][a-z]) \(\(Vol\. \d*\(\.\d*\)? \)?\(Ch. \d*\(\.\d*\)?\)? - \)?*\[*\]
```

Or basically

```regex
(lang) [[Vol. volume ][Ch. chapter ]- ]title \[group_1[, group_2][, group_3]\]
```

Note that it can also have a `/` (folder suffix), `.cbt` or `.cbz` (comic archive) suffix attached.

Yeah. Basically just dumps a load of folders or files into your workspace.

Run where you want files in $PWD; basically.

## Internal commands

These commands are used internally and can have unexpected results if run by the user.

```shell
$ mangadex-curl $@
```

A simple binding for curl. Echos `Waiting on network...` to stderr before running curl.

```shell
$ set_mangadex_group_id $id "$name"
```

Sets `mangadex_groups[$id]` to `$name` if it isn't set.

```shell
$ get_mangadex_group_ids "$JSON"
```

Gets mangadex group ids from `.chapter` on JSON and sets them on `mangadex_groups` automatically.

